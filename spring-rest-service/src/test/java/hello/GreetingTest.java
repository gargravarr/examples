package hello;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hello.bean.PrimeChecker;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GreetingTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void testGreeting() throws Exception {
        mockMvc.perform(get("/greeting").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(
                                MediaType.APPLICATION_JSON_VALUE
                                        + ";charset=UTF-8"))
                .andExpect(jsonPath("id").value(1))
                .andExpect(jsonPath("number").value("Not set"))
                .andExpect(jsonPath("content").value("Hello World!"));
        mockMvc.perform(get("/greeting").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(
                                MediaType.APPLICATION_JSON_VALUE
                                        + ";charset=UTF-8"))
                .andExpect(jsonPath("id").value(2))
                .andExpect(jsonPath("number").value("Not set"))
                .andExpect(jsonPath("content").value("Hello World!"));
    }

    @Test
    public void testGreetingWithParameters() throws Exception {
        mockMvc.perform(
                get("/greeting?name={name}&number={number}", "Rodrigo", 7)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(
                                MediaType.APPLICATION_JSON_VALUE
                                        + ";charset=UTF-8"))
                .andExpect(jsonPath("id").value(3))
                .andExpect(jsonPath("number").value("7 is prime! Hooray!"))
                .andExpect(jsonPath("content").value("Hello Rodrigo!"));
    }

    @Configuration
    @EnableWebMvc
    public static class TestConfiguration {

        @Bean
        public PrimeChecker primeChecker() {
            return new PrimeChecker();
        }

        @Bean
        public GreetingController greetingController() {
            return new GreetingController();
        }
    }
}
