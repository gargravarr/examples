package hello;

import hello.bean.PrimeChecker;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GreetingController {

    private static final String template = "Hello %s!";

    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private PrimeChecker primeChecker;

    @RequestMapping("/greeting")
    public @ResponseBody
    Greeting greeting(
            @RequestParam(value = "name", required = false, defaultValue = "World") String name,
            @RequestParam(value = "number", required = false) Integer number) {
        return new Greeting(counter.incrementAndGet(), String.format(template,
                name), number, primeChecker);
    }
}
