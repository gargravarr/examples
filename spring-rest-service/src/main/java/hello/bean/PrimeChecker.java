package hello.bean;

public class PrimeChecker {

    public String isItPrime(int number) {
        if (number <= 0)
            return number + " is NOT prime!";

        for (int divisor = 2; divisor < number / 2; divisor++) {
            if (number % divisor == 0)
                return number + " is NOT prime!";
        }
        return number + " is prime! Hooray!";
    }
}
