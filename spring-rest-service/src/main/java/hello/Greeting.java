package hello;

import hello.bean.PrimeChecker;

public class Greeting {

    private final long id;

    private final String content;

    private final Integer number;

    private PrimeChecker primeChecker;

    public Greeting(long id, String content, Integer number,
            PrimeChecker primeChecker) {
        this.id = id;
        this.content = content;
        this.number = number;
        this.primeChecker = primeChecker;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getNumber() {
        if (number == null) {
            return "Not set";
        }

        return primeChecker.isItPrime(number);
    }
}
