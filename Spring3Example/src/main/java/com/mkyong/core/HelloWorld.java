package com.mkyong.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Spring bean
 * 
 */
public class HelloWorld {
    private final static Logger LOG = LoggerFactory.getLogger(HelloWorld.class);

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void logHello() {
        LOG.info("Spring 4 : Hello ! " + name);
    }
}
