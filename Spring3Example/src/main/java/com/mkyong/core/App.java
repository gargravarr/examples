package com.mkyong.core;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml");

        HelloWorld obj = (HelloWorld) context.getBean("helloBean");

        obj.logHello();
        context.close();
    }
}
