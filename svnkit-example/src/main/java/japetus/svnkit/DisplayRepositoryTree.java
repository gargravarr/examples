package japetus.svnkit;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;

public class DisplayRepositoryTree {
    private final static Logger LOG = LoggerFactory
            .getLogger(DisplayRepositoryTree.class);

    public static void main(String[] args) {

        // Load svn root from application.properties
        Properties applicationProperties = new Properties();
        try (InputStream in = DisplayRepositoryTree.class
                .getResourceAsStream("/application.properties")) {
            applicationProperties.load(in);
        } catch (IOException e) {
            LOG.error("Unable to load application.properties.", e);
            return;
        }
        String svnRoot = applicationProperties.getProperty("svn.repository");

        DAVRepositoryFactory.setup();

        SVNRepository repository = null;
        try {
            // Connect to repository
            repository = SVNRepositoryFactory.create(SVNURL
                    .parseURIEncoded(svnRoot));

            LOG.info("Repository root: {}", repository.getRepositoryRoot(true));
            LOG.info("Repository UUID: {}", repository.getRepositoryUUID(true));

            // Verify that we're dealing with a directory
            SVNNodeKind nodeKind = repository.checkPath("", -1);
            if (nodeKind == SVNNodeKind.NONE) {
                LOG.error("There is no entry at '{}'.", svnRoot);
                return;
            } else if (nodeKind == SVNNodeKind.FILE) {
                LOG.error(
                        "The entry at '{}' is a file while a directory was expected.",
                        svnRoot);
                return;
            }

            // List the directory contents
            listEntries(repository, "");

            LOG.info("Repository latest revision: {}",
                    repository.getLatestRevision());
        } catch (SVNException e) {
            LOG.error("Error while accessing SVN repository.", e);
            return;
        }
    }

    public static void listEntries(SVNRepository repository, String path)
            throws SVNException {
        Collection<SVNDirEntry> entries = repository.getDir(path, -1, null,
                SVNDirEntry.DIRENT_ALL, (Collection<SVNDirEntry>) null);

        // Recursively traverse all dirs
        for (SVNDirEntry entry : entries) {
            LOG.info("/{}{} ( author: '{}'; revision: {}; date: {})",
                    path.equals("") ? "" : path + "/", entry.getName(),
                    entry.getAuthor(), entry.getRevision(), entry.getDate());

            // If the entry is a dir, traverse it
            if (entry.getKind() == SVNNodeKind.DIR) {
                listEntries(repository, (path.equals("")) ? entry.getName()
                        : path + "/" + entry.getName());
            }
        }
    }
}
