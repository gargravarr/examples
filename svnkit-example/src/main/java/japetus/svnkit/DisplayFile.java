package japetus.svnkit;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNProperty;
import org.tmatesoft.svn.core.SVNPropertyValue;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;

public class DisplayFile {
    private final static Logger LOG = LoggerFactory
            .getLogger(DisplayFile.class);

    public static void main(String[] args) {

        // Load svn root from application.properties
        Properties applicationProperties = new Properties();
        try (InputStream in = DisplayFile.class
                .getResourceAsStream("/application.properties")) {
            applicationProperties.load(in);
        } catch (IOException e) {
            LOG.error("Unable to load application.properties.", e);
            return;
        }
        String svnRoot = applicationProperties.getProperty("svn.repository");
        String filePath = applicationProperties.getProperty("svn.filePath");

        DAVRepositoryFactory.setup();

        SVNRepository repository = null;
        try (ByteArrayOutputStream fileStream = new ByteArrayOutputStream()) {
            // Connect to repository
            repository = SVNRepositoryFactory.create(SVNURL
                    .parseURIEncoded(svnRoot));

            LOG.info("Repository root: {}", repository.getRepositoryRoot(true));
            LOG.info("Repository UUID: {}", repository.getRepositoryUUID(true));

            // Verify that we're dealing with a directory
            SVNNodeKind nodeKind = repository.checkPath(filePath, -1);
            if (nodeKind == SVNNodeKind.NONE) {
                LOG.error("There is no entry at '{}'.", svnRoot);
                return;
            } else if (nodeKind == SVNNodeKind.DIR) {
                LOG.error(
                        "The entry at '{}' is a directory while a file was expected.",
                        svnRoot);
                return;
            }

            SVNProperties fileProperties = new SVNProperties();
            repository.getFile(filePath, -1, fileProperties, fileStream);

            for (String property : fileProperties.nameSet()) {
                SVNPropertyValue propertyValue = fileProperties
                        .getSVNPropertyValue(property);
                LOG.info("File property: {} = {}", property, propertyValue);
            }

            if (SVNProperty.isTextMimeType(fileProperties
                    .getStringValue(SVNProperty.MIME_TYPE))) {
                LOG.info("File contents: \n{}", fileStream.toString());
            }

        } catch (SVNException | IOException e) {
            LOG.error("Error while accessing SVN repository.", e);
            return;
        }
    }
}
