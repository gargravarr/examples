#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'rreis'

from os import system
import curses
import collections

COLOR_CONFIRM = 1
COLOR_ERROR = 2
COLOR_MENU = 3
COLOR_PROGRESS = 4
COLOR_SUCCESS = 5
COLOR_DONE = 6
COLOR_WARNING = 7

HEADER_HEIGHT, FOOTER_HEIGHT = 3, 3
SIDE_MARGIN = 2
MARGINS_HEIGHT, MARGINS_WIDTH = HEADER_HEIGHT + FOOTER_HEIGHT, SIDE_MARGIN * 2
MIN_VPADDING, MIN_HPADDING = 3, 3

Dialog = collections.namedtuple('Dialog', ['win', 'width', 'height', 'body_height'])

def get_param(screen, prompt_string):
    screen.clear()
    screen.border(0)
    screen.addstr(2, 2, prompt_string)
    screen.refresh()

    curses.echo()  # Must be enabled to display the characters entered
    input_string = screen.getstr(10, 10, 60)
    curses.noecho()

    return input_string


def execute_cmd(cmd_string):
    system("clear")
    a = system(cmd_string)
    print ""
    if a == 0:
        print "Command executed correctly"
    else:
        print "Command terminated with error"
    raw_input("Press enter")
    print ""


def setup_select_dialog(screen, items, min_width=0, multi_select=False):
    # Get the current width and height of the console
    height, width = screen.getmaxyx()

    # Calculate vertical padding. Minimum is MIN_VPADDING, unless the list of items doesn't need such a big dialog.
    if len(items) > height - MARGINS_HEIGHT - MIN_VPADDING:
        vpadding = MIN_VPADDING
    else:
        vpadding = (height - MARGINS_HEIGHT - len(items)) / 2

    # Calculate horizontal padding. Relative to the maximum string length found in items and dialog prompt. Minimum is 3
    max_stringlen = max([len(item) + 3 if multi_select else 0 for item in items] + [min_width])
    hpadding = MIN_HPADDING if max_stringlen > width - MARGINS_WIDTH else (width - MARGINS_WIDTH - max_stringlen) / 2

    # Determine measurements of the dialog, based on padding
    dialog_height = height - vpadding * 2 if vpadding == MIN_VPADDING else MARGINS_HEIGHT + len(items)
    body_height = dialog_height - MARGINS_HEIGHT
    dialog_width = width - hpadding * 2 if hpadding == MIN_HPADDING else MARGINS_WIDTH + max_stringlen

    # Initialize dialog
    return Dialog(screen.subwin(dialog_height, dialog_width, vpadding, hpadding), dialog_width, dialog_height,
                  body_height)


def multi_select_dialog(screen, n_items, selected=None):
    title, selection_count = 'Multi-select dialog example', '({}/{} selected)'
    footer = '<UP>/<DOWN>: move, <SPACE>: select/deselect, <A>: select all, <ENTER>: accept selection'
    check, uncheck = '[X]', '[ ]'
    selection_items = ['Item {}'.format(i) for i in range(0, n_items)]

    if selected is None:
        selected = set()

    # Setup dialog
    dialog = setup_select_dialog(screen, selection_items, min_width=len(footer), multi_select=True)

    # Show a view with the selected item on top, unless there's less elements to display in the dialog body
    highlighted, top_item = 0, 0
    while True:
        # Everything must be repainted
        dialog.win.erase()
        # Draw dialog
        dialog.win.bkgdset(' ', curses.color_pair(COLOR_MENU))
        dialog.win.attron(curses.color_pair(COLOR_MENU))
        dialog.win.border(0)

        # Print header. Since the number of selected elements is right justified, calculate the necessary spacing.
        header_right = selection_count.format(len(selected), n_items)
        spacing = ' ' * (dialog.width - len(title) - len(header_right) - MARGINS_WIDTH)
        header = title + spacing + header_right
        dialog.win.addstr(1, 2, header)
        dialog.win.hline(2, 1, curses.ACS_HLINE, dialog.width - 2)

        # Show a slice of the items, based on the defined visible lines
        for index, item in enumerate(selection_items[top_item:top_item + dialog.body_height], start=top_item):
            # Highlight selected item
            highlight = curses.A_REVERSE if index == highlighted else 0
            item_display = '{} {}'.format((check if selection_items[index] in selected else uncheck), item)
            dialog.win.addstr(index - top_item + HEADER_HEIGHT, 2, item_display, highlight)
        dialog.win.refresh()

        # Print footer
        dialog.win.hline(dialog.height - 3, 1, curses.ACS_HLINE, dialog.width - 2)
        dialog.win.addstr(dialog.height - 2, 2, footer)

        dialog.win.refresh()

        # TODO finish key menu (protect from erroneous input)
        # Read input
        key = screen.getch()
        if key == curses.KEY_UP and highlighted > 0:
            if highlighted == top_item:  # Slide up the list of visible elements
                top_item -= 1
            highlighted -= 1
        elif key == curses.KEY_DOWN and highlighted < n_items - 1:
            if highlighted == top_item + dialog.body_height - 1:  # Slide up the list of visible elements
                top_item += 1
            highlighted += 1
        elif key == curses.KEY_PPAGE and highlighted > 0:
            top_item -= dialog.body_height
            if top_item < 0:
                top_item = 0
            highlighted -= dialog.body_height
            if highlighted < 0:
                highlighted = 0
        elif key == curses.KEY_NPAGE and highlighted < n_items - 1:
            top_item += dialog.body_height
            if top_item >= n_items - dialog.body_height:
                top_item = n_items - dialog.body_height
            highlighted += dialog.body_height
            if highlighted >= n_items:
                highlighted = n_items - 1
        elif key == 32:  # SPACE
            if selection_items[highlighted] in selected:
                selected.discard(selection_items[highlighted])
            else:
                selected.add(selection_items[highlighted])
        elif key == ord('a'):
            if selected:
                selected.clear()
            else:
                selected = set(selection_items)
        elif key == 10:  # ENTER
            break

    dialog.win.erase()
    return selected


def single_select_dialog(screen, n_items, selected=0):
    header = 'Scrolling dialog example (Selected: {} Top item: {})'
    footer = '<UP>/<DOWN>: move, <ENTER>: accept selection'
    selection_items = ['Item {}'.format(i) for i in range(0, n_items)]

    # Setup dialog
    dialog = setup_select_dialog(screen, selection_items, min_width=max(len(header), len(footer)))

    # Show a view with the selected item on top, unless there's less elements to display in the dialog body
    top_item = selected if n_items - selected > dialog.body_height else n_items - dialog.body_height
    while True:
        # Everything must be repainted
        dialog.win.erase()
        # Draw dialog
        dialog.win.bkgdset(' ', curses.color_pair(COLOR_MENU))
        dialog.win.attron(curses.color_pair(COLOR_MENU))
        dialog.win.border(0)

        # Print header
        dialog.win.addstr(1, 2, header.format(selected, top_item))
        dialog.win.hline(2, 1, curses.ACS_HLINE, dialog.width - 2)

        # Show a slice of the items, based on the defined visible lines
        for index, item in enumerate(selection_items[top_item:top_item + dialog.body_height], start=top_item):
            # Highlight selected item
            highlight = curses.A_REVERSE if index == selected else 0
            dialog.win.addstr(index - top_item + HEADER_HEIGHT, 2, item, highlight)

        # Print footer
        dialog.win.hline(dialog.height - 3, 1, curses.ACS_HLINE, dialog.width - 2)
        dialog.win.addstr(dialog.height - 2, 2, footer)

        dialog.win.refresh()

        # Read input
        key = screen.getch()
        if key == curses.KEY_UP and selected > 0:
            if selected == top_item:  # Slide up the list of visible elements
                top_item -= 1
            selected -= 1
        elif key == curses.KEY_DOWN and selected < n_items-1:
            if selected == top_item + dialog.body_height - 1:  # Slide up the list of visible elements
                top_item += 1
            selected += 1
        elif key == 10:  # ENTER
            break

    dialog.win.erase()
    return selection_items[selected]


def main(screen):
    x = 0

    init_colors()

    while x != ord('4'):

        screen.clear()
        screen.border(0)
        screen.addstr(2, 2, "Please enter a number...")
        screen.addstr(4, 4, "1 - Grep a file")
        screen.addstr(5, 4, "2 - Scrollable dialog")
        screen.addstr(6, 4, "3 - Multi-select dialog")
        screen.addstr(7, 4, "4 - Exit")
        screen.refresh()

        x = screen.getch()

        if x == ord('1'):
            filename = get_param(screen, "Enter the file to grep")
            search_text = get_param(screen, "Enter the text to search")
            curses.endwin()
            execute_cmd("grep " + search_text + " " + filename)
        if x == ord('2'):
            n_items = int(get_param(screen, "How many items?"))
            pre_selected = int(get_param(screen, "Which preselected item? (0 to {})".format(n_items)))
            screen.clear()
            selected = single_select_dialog(screen, n_items, selected=pre_selected)
            curses.endwin()
            execute_cmd("echo Selected item: {}".format(selected))
        if x == ord('3'):
            n_items = int(get_param(screen, "How many items?"))
            screen.clear()
            selected = multi_select_dialog(screen, n_items)
            curses.endwin()
            execute_cmd('echo Selected items: "{}"'.format(selected))

    curses.endwin()


def init_colors():
    """Initialize curses colors.

    :return: None
    """

    curses.init_pair(COLOR_CONFIRM, curses.COLOR_WHITE, curses.COLOR_BLUE)
    curses.init_pair(COLOR_ERROR, curses.COLOR_WHITE, curses.COLOR_RED)
    curses.init_pair(COLOR_MENU, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(COLOR_PROGRESS, curses.COLOR_WHITE, curses.COLOR_YELLOW)
    curses.init_pair(COLOR_SUCCESS, curses.COLOR_WHITE, curses.COLOR_GREEN)
    curses.init_pair(COLOR_DONE, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(COLOR_WARNING, curses.COLOR_BLACK, curses.COLOR_YELLOW)


if __name__ == '__main__':
    curses.wrapper(main)
