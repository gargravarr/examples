#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'rreis'

import curses


class MenuDemo:
    KEY = {
        'SPACE': 32,
        'ESC': 27
    }

    INCREMENT = {
        'DOWN': 1,
        'UP': -1
    }

    SELECTED = '_X_'
    DESELECTED = '___'

    outputLines = []
    nOutputLines = 0
    screen = None

    def __init__(self):
        self.screen = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.screen.keypad(1)
        self.screen.border(0)
        self.topLineNum = 0
        self.highlightLineNum = 0
        self.markedLineNums = []
        self.get_output_lines()
        self.run()

    def run(self):
        while True:
            self.get_display_screen()
            # get user command
            c = self.screen.getch()
            if c == curses.KEY_UP:
                self.updown(self.INCREMENT['UP'])
            elif c == curses.KEY_DOWN:
                self.updown(self.INCREMENT['DOWN'])
            elif c == self.KEY['SPACE']:
                self.mark_line()
            elif c == self.KEY['ESC']:
                self.exit()

    def mark_line(self):
        linenum = self.topLineNum + self.highlightLineNum
        if linenum in self.markedLineNums:
            self.markedLineNums.remove(linenum)
        else:
            self.markedLineNums.append(linenum)

    def get_output_lines(self):
        self.outputLines = ['Line {}'.format(i) for i in range(0, 100)]
        self.nOutputLines = len(self.outputLines)

    def get_display_screen(self):
        # clear screen
        self.screen.erase()

        # now paint the rows
        top = self.topLineNum
        bottom = self.topLineNum + curses.LINES
        for (index, line,) in enumerate(self.outputLines[top:bottom]):
            linenum = self.topLineNum + index
            prefix = self.SELECTED if linenum in self.markedLineNums else self.DESELECTED

            line = '{} {}'.format(prefix, line)

            # highlight current line
            if index != self.highlightLineNum:
                self.screen.addstr(index, 0, line)
            else:
                self.screen.addstr(index, 0, line, curses.A_BOLD)
        self.screen.refresh()

    # move highlight up/down one line
    def updown(self, increment):
        nextLineNum = self.highlightLineNum + increment

        # paging
        if increment == self.INCREMENT['UP'] and self.highlightLineNum == 0 and self.topLineNum != 0:
            self.topLineNum += self.INCREMENT['UP']
            return
        elif increment == self.INCREMENT['DOWN'] and nextLineNum == curses.LINES and (
                    self.topLineNum + curses.LINES) != self.nOutputLines:
            self.topLineNum += self.INCREMENT['DOWN']
            return

        # scroll highlight line
        if increment == self.INCREMENT['UP'] and (self.topLineNum != 0 or self.highlightLineNum != 0):
            self.highlightLineNum = nextLineNum
        elif increment == self.INCREMENT['DOWN'] and self.highlightLineNum != curses.LINES and (
                        self.topLineNum + self.highlightLineNum + 1) != self.nOutputLines:
            self.highlightLineNum = nextLineNum

    def restore_screen(self):
        curses.initscr()
        curses.nocbreak()
        curses.echo()
        curses.endwin()

    # catch any weird termination situations
    def __del__(self):
        self.restore_screen()


if __name__ == '__main__':
    ih = MenuDemo()