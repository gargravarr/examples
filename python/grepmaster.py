#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'rreis'

from os import system
import curses


def get_param(prompt_string):
    screen.clear()
    screen.border(0)
    screen.addstr(2, 2, prompt_string)
    screen.refresh()
    input_string = screen.getstr(10, 10, 60)
    return input_string


def execute_cmd(cmd_string):
    system("clear")
    a = system(cmd_string)
    print ""
    if a == 0:
        print "Command executed correctly"
    else:
        print "Command terminated with error"
    raw_input("Press enter")
    print ""


x = 0

while x != ord('3'):
    screen = curses.initscr()

    screen.clear()
    screen.border(0)
    screen.addstr(2, 2, "Please enter a number...")
    screen.addstr(4, 4, "1 - Grep a file")
    screen.addstr(5, 4, "2 - Show disk space")
    screen.addstr(6, 4, "3 - Exit")
    screen.refresh()

    x = screen.getch()

    if x == ord('1'):
        filename = get_param("Enter the file to grep")
        search_text = get_param("Enter the text to search")
        curses.endwin()
        execute_cmd("grep " + search_text + " " + filename)
    if x == ord('2'):
        curses.endwin()
        execute_cmd("df -h")

curses.endwin()